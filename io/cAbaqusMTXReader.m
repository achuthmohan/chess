classdef cAbaqusMTXReader<cReader
    %Sub Class for Abaqus MTX Reader
    %   Author: Achuth
    
    properties
        pType
        pRead
        pSparseMat
    end
    
    methods
        function obj = cAbaqusMTXReader(afilename,atype)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj@cReader(afilename);
            obj.pType = atype;
            display('> Abaqus MTX reader initialized.')
        end
        
        function obj = readMTX(obj)
            if obj.pType == "COORDINATE"
                display('> Reading input...')
                obj.pRead = load(obj.pFilename);
                display('> Reading input... Finished.')
            else
                error('> Undefined reader type');
            end
        end
        
        function obj = makeSparse(obj)
            if obj.pType == "INPUT"
                obj.pSparseMat = spconvert(obj.pRead);
                display(['> Sparse Matrix of size ',num2str(size(obj.pSparseMat,1)),'x',num2str(size(obj.pSparseMat,1)),' created'])
            else
                error('> Undefined reader type');
            end
        end
        
        function obj = spySparseMat(obj)
            figure;
            spy(obj.pSparseMat);
        end
        
        function nDOF = getDimension(obj)
            nDOF = size(obj.pSparseMat,1);
        end
    end
end

