classdef cReader<handle
    %Super Class for Reader File handler
    %   Author: Achuth
    
    properties
        pFilename
    end
    
    methods
        function obj = cReader(afilename)
            obj.pFilename = afilename;
        end
    end
end

