clear all;
close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CHESS: aChutH Elementary Sparse Solver %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% Main Flow %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Achuth, Braunschweig %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
addpath('./analysis','./io','./mtxfiles','./load')

%% Read Abaqus Matrix Exports
% Stiffness
stiffnessMat = cAbaqusMTXReader('PlateExport_STIF1.mtx','COORDINATE');
stiffnessMat.readMTX();
stiffnessMat.makeSparse();

% Mass
massMat = cAbaqusMTXReader('PlateExport_MASS1.mtx','COORDINATE');
massMat.readMTX();
massMat.makeSparse();

% structural Damping
sdMat = cAbaqusMTXReader('PlateExport_DMPS1.mtx','COORDINATE');
sdMat.readMTX();
sdMat.makeSparse();

%stiffnessMat.spySparseMat();

%% Analysis
nDOF = stiffnessMat.getDimension();
myLoad = cLoad(nDOF);
myLoad.addConcentratedLoad(3,1);
% Modal Analysis
myModalAnalysis = cModal('MyModalAnalysis');
myModalAnalysis.setSystemMatrices(stiffnessMat.pSparseMat+1i*sdMat.pSparseMat,massMat.pSparseMat,sparse(nDOF,nDOF));
myModalAnalysis.setAnalysisParameters(50,'smallestabs');
myModalAnalysis.performEigenValueAnalysis();
% SSD Analysis
mySSDAnalysis = cSSD('MySSDAnalysis');
mySSDAnalysis.setSystemMatrices(stiffnessMat.pSparseMat+1i*sdMat.pSparseMat,massMat.pSparseMat,sparse(nDOF,nDOF));
mySSDAnalysis.performSteadyStateAnalysis(20:1:100,myLoad.pRHS);