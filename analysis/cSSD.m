classdef cSSD<cAnalysis
    %Sub Class for SSD Analysis
    %   Author: Achuth
    
    properties
        pFrequency
        pSol
    end
    
    methods
        function obj = cSSD(aAnalysisName)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj@cAnalysis(aAnalysisName,"SSD");
            display('> Steady-State Analysis successfully created.')
        end
        
        function obj = performSteadyStateAnalysis(obj,aFreqrange,aLoad)
            display('> Performing Steady-State Analysis...')
            tic
            obj.pFrequency = aFreqrange;
            for iFreq = 1:length(obj.pFrequency)
                display([' > Solving at frequency ', num2str(obj.pFrequency(iFreq)),' Hz'])
                omega = 2 * pi *obj.pFrequency(iFreq);
                K_dyn = (-omega^2*obj.pMMAT-1i*omega*obj.pDMAT+obj.pKMAT);
                obj.pSol = [obj.pSol K_dyn\aLoad];
            end
            t_SSD= toc;
            display(['> Performing Steady-State Analysis... Finished in ',num2str(t_SSD),' secs']);
        end
    end
end

