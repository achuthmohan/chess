classdef cModal<cAnalysis
    %Sub Class for Modal Analysis
    %   Author: Achuth
    
    properties
        pNumModes
        pModeProps
        pEigenVal
        pEigenVec
        pEigenFreq
    end
    
    methods
        function obj = cModal(aAnalysisName)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj@cAnalysis(aAnalysisName,"MODAL");
            
            display('> Modal Analysis successfully created.')
        end
        
        function obj = setAnalysisParameters(obj,aNumModes,aSigma)
            obj.pNumModes = aNumModes;
            obj.pModeProps = aSigma;
        end
        
        function obj = performEigenValueAnalysis(obj)
            display('> Performing Eigen Value Analysis...')
            tic
            [obj.pEigenVec,obj.pEigenVal] = eigs(obj.pKMAT,obj.pMMAT,obj.pNumModes,obj.pModeProps);
            obj.pEigenVal=diag(obj.pEigenVal);
            obj.pEigenFreq = sqrt(obj.pEigenVal)/(2*pi);
            t_EIG= toc;
            display(['> Performing Eigen Value Analysis... Finished in ',num2str(t_EIG),' secs']);
        end 
    end
end

