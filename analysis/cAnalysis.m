classdef cAnalysis<handle
    %Super Class for Analysis handler
    %   Author: Achuth
    
    properties
        pAnalysisName
        pAnalysisType
        pAnalysis
        pKMAT
        pMMAT
        pDMAT
    end
    
    methods
        function obj = cAnalysis(aAnalysisName,aAnalysisType)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            obj.pAnalysisName = aAnalysisName;
            obj.pAnalysisType = aAnalysisType;
            
        end
        
        function obj = setSystemMatrices(obj,aKMAT,aMMAT,aDMAT)
            obj.pKMAT = aKMAT;
            obj.pMMAT = aMMAT;
            obj.pDMAT = aDMAT;
        end
    end
end

