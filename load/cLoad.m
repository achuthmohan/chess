classdef cLoad<handle
    %Super Class for Load handler
    %   Author: Achuth
    
    properties
        pRHS
    end
    
    methods
        function obj = cLoad(aSize)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            obj.pRHS = zeros(aSize,1);
            display('> Load vector created.')
        end
        
        function obj = addConcentratedLoad(obj, aDof, aValue)
            obj.pRHS(aDof,1) = aValue;
        end
    end
end

